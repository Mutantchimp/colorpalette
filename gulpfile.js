var gulp = require('gulp'),
	sass = require('gulp-sass'),
	browserify = require('browserify'),
	vinyl = require('vinyl-source-stream'),
	buffer = require('vinyl-buffer'),
	babel = require('gulp-babel'),
	jshint = require('gulp-jshint'),
	uglify = require('gulp-uglify'),
	publicDir = 'public/',
	sourceDir = 'source/';

gulp.task('sass', function() {
    return gulp.src(sourceDir + '/scss/*.scss')
        .pipe(sass())
        .pipe(gulp.dest(publicDir + '/css'));
});

gulp.task('lint-source', function() {
    return gulp.src(sourceDir + 'js/*.js')
        .pipe(jshint({
        	esversion: 6,
        	jquery:true,
        	browserify:true
        }))
        .pipe(jshint.reporter('default'));
});

gulp.task('js-dev', function () {
	return (browserify(sourceDir + '/js/main.js')
		.bundle()
		.pipe(vinyl('main.js'))
		.pipe(buffer())
		.pipe(babel({
			presets:['es2015']
		}))
		//.pipe(uglify())
		.pipe(gulp.dest(publicDir + '/js')))
});
gulp.task('js-production', function () {
	return (browserify(sourceDir + '/js/main.js')
		.bundle()
		.pipe(vinyl('main.js'))
		.pipe(buffer())
		.pipe(babel({
			presets:['es2015']
		}))
		.pipe(uglify())
		.pipe(gulp.dest(publicDir + '/js')))
});