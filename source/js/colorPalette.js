'use strict';
var ColorUtil = require('./colorUtil.js'),
    colorUtil = new ColorUtil();
// ColorPalette takes a base color in hex, converts it to hsv for manipulation, the adds a 
// series of related colors based on google's material design, to an array.
// in the array reach related color should have hex, hsv, and rgb properties.
var ColorPalette = function(baseColor,$element,baseCssClass) {
  this.baseColor = baseColor;
  this.colorVersions = [];
  this.cssClassArray = [];
  var baseCssClass = baseCssClass;
  if (!baseCssClass) {
    this.baseCssClass = 'primaryColor';
  }

  // checking to make sure baseColor is hex
  this.processColors = function(color) {
    if (typeof color === 'string' && color.length <= 7){
        // remove pound if included
        if (color.indexOf('#') > 0){color = color.slice(color.indexOf('#')); };
        color = colorUtil.hexToRgb(color);
        color = colorUtil.rgb2hsv(color.r, color.g, color.b);
    }
    //for (var i = this.vModifiers.length-1; i >= 0; i--){
    //    var rColor = this.variation(color,this.vModifiers[i]);
    //    this.colorVersions.push(rColor);
    //}
    this.variation(color,this.vModifiers);
    this.createCss(this.baseCssClass, this.colorVersions);
    this.drawBoxes(this.cssClassArray,$element, this.colorVersions,this.baseCssClass);
  };

  // sets up the sliders
  this.controlColorProcess = function($element){
    var controls = $element.find('.colorPaletteControl'),
        generateButton = $element.find('#generatePaletteButton'),
        hField = controls.find('#colorPaletteHValue'),
        sField = controls.find('#colorPaletteSValue'),
        vField = controls.find('#colorPaletteVValue'),
        hexField = controls.find('#colorPaletteHexValue'),
        preview = controls.find('#colorPreviewSwatch'),
        controlColor = {h: hField.val(), s: sField.val(), v: vField.val()};
    controlColor = colorUtil.hsvToRgb(controlColor.h, controlColor.s, controlColor.v);
    controlColor = colorUtil.rgbToHex(controlColor.r, controlColor.g, controlColor.b);
    hexField.val(controlColor);
    preview.css('background-color',controlColor);
  }

  // inits the sliders.
  this.initControls = function($element) {
    this.controlColorProcess($element);
    var that = this;
    $element.on('mouseup keyup', function (event){
      that.controlColorProcess($element);
    });
    $('#generatePaletteButton').on('click', function(event){
      var hexFieldVal = $element.find('#colorPaletteHexValue').val();
      that.processColors(hexFieldVal);
    });
  };
  this.processColors(this.baseColor);
  this.initControls($element);
};

// this sets up an array of modifier values.  For percentage changes use a
// a string value, for degrees or interger changes use a interger value
ColorPalette.prototype.vModifiers = [
  {
    name: 'v500',
    h: null,
    s: null,
    v: null,
  },
  {
    name: 'v400',
    h: null,
    s: '-20%',
    v: '7%'
  },
  {
    name:'v300',
    h: null,
    s: '-40%',
    v: '13%'
  },
  {
    name: 'v200',
    h: null,
    s: '-59%',
    v: '23%'
  },
  {
    name: 'v100',
    h: null,
    s: '-77%',
    v: '31%'
  },
  {
    name: 'v50',
    h: null,
    s: '-99%',
    v: '40%'
  },
  {
    name: 'v600',
    h: '-2%',
    s: '1%',
    v: '-3%'
  },
  {
    name: 'v700',
    h: '-3%',
    s: '3%',
    v: '-8%'
  },
  {
    name: 'v800',
    h: '-5%',
    s: '5%',
    v: '-13%'
  },
  {
    name:'v900',
    h: '-9%',
    s: '10%',
    v: '-21%'
  },
  {
    name:'a1',
    h: null,
    s: '-38%',
    v: '43%'
  },
  {
    name:'a2',
    h: null,
    s: '-4%',
    v: '42%'
  }
];



ColorPalette.prototype.variation = function (oColor,modifiers) {
  this.colorVersions = [];
  for (var i = modifiers.length-1; i >= 0; i--){
    var oColorRGB = {},
        newColor = {},
        newHSVValues = modifiers[i];
    for (var prop in newHSVValues){
      if (prop == 'name') {
        newColor[prop] = newHSVValues[prop];
        continue;
      }
      if (typeof newHSVValues[prop] === 'string' && newHSVValues[prop].indexOf('%') >= 0){
        newColor[prop] = oColor[prop] + (Math.round(oColor[prop] * (parseFloat(newHSVValues[prop]) / 100)));
      }
      if (typeof newHSVValues[prop] === 'number'){
        newColor[prop] = oColor[prop] + newHSVValues[prop];
      }
      if (!newHSVValues[prop]){
        newColor[prop] = oColor[prop];
      }
    }
    
    if (newColor.v > 100){
      newColor.s -= (newColor.v - 100)/10;
      newColor.v = 100;
    }
    newColor.s = Math.max(newColor.s, 10);
    console.log(newColor.name + ' s: ' + newColor.s + ' v: ' + newColor.v);
    oColorRGB = colorUtil.hsvToRgb(newColor.h, newColor.s, newColor.v);
    newColor.hex = colorUtil.rgbToHex(oColorRGB.r, oColorRGB.g, oColorRGB.b);
    newColor.r = oColorRGB.r;
    newColor.g = oColorRGB.g;
    newColor.b = oColorRGB.b;
    this.colorVersions.push(newColor);
  }
};

ColorPalette.prototype.createCss = function(baseCSSClass, colorArray) {
  this.cssClassArray = [];
  for (var i = colorArray.length - 1; i >= 0; i--){
   this.cssClassArray[i] = '.' + baseCSSClass + colorArray[i].name + '{\n\tcolor: ' + colorArray[i].hex + '\n}\n';
  }
};
ColorPalette.prototype.drawBoxes = function(cssClassArray,$element, colorArray,baseCSSClass) {
  var swatchHTML = '<div class="colorPalatteSwatches">',
      cssHTML = '';
  for (var i = cssClassArray.length -1; i >=0; i--){
    cssHTML += cssClassArray[i];
  };
  //swatchHTML += '<style>' + cssHTML + '</style>';
  for (var i = colorArray.length -1; i >= 0; i--){
    swatchHTML += '<div class="colorPaletteSwatch ' + baseCSSClass + colorArray[i].name +'" style="background-color:' + colorArray[i].hex +'">'+ colorArray[i].name +'</div>';
  };
  swatchHTML += '</div>';
  $element.find('.colorPalettePreview').html(swatchHTML);
  $element.find('.colorPaletteCSS').html('<pre>' + cssHTML + '</pre>');
};

module.exports = ColorPalette;



