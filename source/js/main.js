'use strict'
require('./foundation/foundation.core.js');
require('./foundation/foundation.util.mediaQuery.js');
require('./foundation/foundation.util.motion.js');
require('./foundation/foundation.util.triggers.js');
require('./foundation/foundation.util.keyboard.js');
require('./foundation/foundation.util.touch.js');
require('./foundation/foundation.slider.js');
var ColorPalette = require('./colorPalette.js');


$(document).ready(function(){
	$(document).foundation();
	window.colorPaletteObject = new ColorPalette('9c27b0',$('#colorPalette'));
});