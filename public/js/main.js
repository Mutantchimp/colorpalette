"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol ? "symbol" : typeof obj; };

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(function e(t, n, r) {
  function s(o, u) {
    if (!n[o]) {
      if (!t[o]) {
        var a = typeof require == "function" && require;if (!u && a) return a(o, !0);if (i) return i(o, !0);var f = new Error("Cannot find module '" + o + "'");throw f.code = "MODULE_NOT_FOUND", f;
      }var l = n[o] = { exports: {} };t[o][0].call(l.exports, function (e) {
        var n = t[o][1][e];return s(n ? n : e);
      }, l, l.exports, e, t, n, r);
    }return n[o].exports;
  }var i = typeof require == "function" && require;for (var o = 0; o < r.length; o++) {
    s(r[o]);
  }return s;
})({ 1: [function (require, module, exports) {
    'use strict';

    var ColorUtil = require('./colorUtil.js'),
        colorUtil = new ColorUtil();
    // ColorPalette takes a base color in hex, converts it to hsv for manipulation, the adds a
    // series of related colors based on google's material design, to an array.
    // in the array reach related color should have hex, hsv, and rgb properties.
    var ColorPalette = function ColorPalette(baseColor, $element, baseCssClass) {
      this.baseColor = baseColor;
      this.colorVersions = [];
      this.cssClassArray = [];
      var baseCssClass = baseCssClass;
      if (!baseCssClass) {
        this.baseCssClass = 'primaryColor';
      }

      // checking to make sure baseColor is hex
      this.processColors = function (color) {
        if (typeof color === 'string' && color.length <= 7) {
          // remove pound if included
          if (color.indexOf('#') > 0) {
            color = color.slice(color.indexOf('#'));
          };
          color = colorUtil.hexToRgb(color);
          color = colorUtil.rgb2hsv(color.r, color.g, color.b);
        }
        //for (var i = this.vModifiers.length-1; i >= 0; i--){
        //    var rColor = this.variation(color,this.vModifiers[i]);
        //    this.colorVersions.push(rColor);
        //}
        this.variation(color, this.vModifiers);
        this.createCss(this.baseCssClass, this.colorVersions);
        this.drawBoxes(this.cssClassArray, $element, this.colorVersions, this.baseCssClass);
      };

      // sets up the sliders
      this.controlColorProcess = function ($element) {
        var controls = $element.find('.colorPaletteControl'),
            generateButton = $element.find('#generatePaletteButton'),
            hField = controls.find('#colorPaletteHValue'),
            sField = controls.find('#colorPaletteSValue'),
            vField = controls.find('#colorPaletteVValue'),
            hexField = controls.find('#colorPaletteHexValue'),
            preview = controls.find('#colorPreviewSwatch'),
            controlColor = { h: hField.val(), s: sField.val(), v: vField.val() };
        controlColor = colorUtil.hsvToRgb(controlColor.h, controlColor.s, controlColor.v);
        controlColor = colorUtil.rgbToHex(controlColor.r, controlColor.g, controlColor.b);
        hexField.val(controlColor);
        preview.css('background-color', controlColor);
      };

      // inits the sliders.
      this.initControls = function ($element) {
        this.controlColorProcess($element);
        var that = this;
        $element.on('mouseup keyup', function (event) {
          that.controlColorProcess($element);
        });
        $('#generatePaletteButton').on('click', function (event) {
          var hexFieldVal = $element.find('#colorPaletteHexValue').val();
          that.processColors(hexFieldVal);
        });
      };
      this.processColors(this.baseColor);
      this.initControls($element);
    };

    // this sets up an array of modifier values.  For percentage changes use a
    // a string value, for degrees or interger changes use a interger value
    ColorPalette.prototype.vModifiers = [{
      name: 'v500',
      h: null,
      s: null,
      v: null
    }, {
      name: 'v400',
      h: null,
      s: '-20%',
      v: '7%'
    }, {
      name: 'v300',
      h: null,
      s: '-40%',
      v: '13%'
    }, {
      name: 'v200',
      h: null,
      s: '-59%',
      v: '23%'
    }, {
      name: 'v100',
      h: null,
      s: '-77%',
      v: '31%'
    }, {
      name: 'v50',
      h: null,
      s: '-99%',
      v: '40%'
    }, {
      name: 'v600',
      h: '-2%',
      s: '1%',
      v: '-3%'
    }, {
      name: 'v700',
      h: '-3%',
      s: '3%',
      v: '-8%'
    }, {
      name: 'v800',
      h: '-5%',
      s: '5%',
      v: '-13%'
    }, {
      name: 'v900',
      h: '-9%',
      s: '10%',
      v: '-21%'
    }, {
      name: 'a1',
      h: null,
      s: '-38%',
      v: '43%'
    }, {
      name: 'a2',
      h: null,
      s: '-4%',
      v: '42%'
    }];

    ColorPalette.prototype.variation = function (oColor, modifiers) {
      this.colorVersions = [];
      for (var i = modifiers.length - 1; i >= 0; i--) {
        var oColorRGB = {},
            newColor = {},
            newHSVValues = modifiers[i];
        for (var prop in newHSVValues) {
          if (prop == 'name') {
            newColor[prop] = newHSVValues[prop];
            continue;
          }
          if (typeof newHSVValues[prop] === 'string' && newHSVValues[prop].indexOf('%') >= 0) {
            newColor[prop] = oColor[prop] + Math.round(oColor[prop] * (parseFloat(newHSVValues[prop]) / 100));
          }
          if (typeof newHSVValues[prop] === 'number') {
            newColor[prop] = oColor[prop] + newHSVValues[prop];
          }
          if (!newHSVValues[prop]) {
            newColor[prop] = oColor[prop];
          }
        }

        if (newColor.v > 100) {
          newColor.s -= (newColor.v - 100) / 10;
          newColor.v = 100;
        }
        newColor.s = Math.max(newColor.s, 10);
        console.log(newColor.name + ' s: ' + newColor.s + ' v: ' + newColor.v);
        oColorRGB = colorUtil.hsvToRgb(newColor.h, newColor.s, newColor.v);
        newColor.hex = colorUtil.rgbToHex(oColorRGB.r, oColorRGB.g, oColorRGB.b);
        newColor.r = oColorRGB.r;
        newColor.g = oColorRGB.g;
        newColor.b = oColorRGB.b;
        this.colorVersions.push(newColor);
      }
    };

    ColorPalette.prototype.createCss = function (baseCSSClass, colorArray) {
      this.cssClassArray = [];
      for (var i = colorArray.length - 1; i >= 0; i--) {
        this.cssClassArray[i] = '.' + baseCSSClass + colorArray[i].name + '{\n\tcolor: ' + colorArray[i].hex + '\n}\n';
      }
    };
    ColorPalette.prototype.drawBoxes = function (cssClassArray, $element, colorArray, baseCSSClass) {
      var swatchHTML = '<div class="colorPalatteSwatches">',
          cssHTML = '';
      for (var i = cssClassArray.length - 1; i >= 0; i--) {
        cssHTML += cssClassArray[i];
      };
      //swatchHTML += '<style>' + cssHTML + '</style>';
      for (var i = colorArray.length - 1; i >= 0; i--) {
        swatchHTML += '<div class="colorPaletteSwatch ' + baseCSSClass + colorArray[i].name + '" style="background-color:' + colorArray[i].hex + '">' + colorArray[i].name + '</div>';
      };
      swatchHTML += '</div>';
      $element.find('.colorPalettePreview').html(swatchHTML);
      $element.find('.colorPaletteCSS').html('<pre>' + cssHTML + '</pre>');
    };

    module.exports = ColorPalette;
  }, { "./colorUtil.js": 2 }], 2: [function (require, module, exports) {
    'use strict';

    // color utilities from very nice anonymous people

    var ColorUtil = function ColorUtil() {};

    ColorUtil.prototype.componentToHex = function (c) {
      var hex = c.toString(16);
      return hex.length == 1 ? "0" + hex : hex;
    };

    ColorUtil.prototype.rgbToHex = function (r, g, b) {
      return "#" + this.componentToHex(r) + this.componentToHex(g) + this.componentToHex(b);
    };

    ColorUtil.prototype.hexToRgb = function (hex) {
      // Expand shorthand form (e.g. "03F") to full form (e.g. "0033FF")
      var shorthandRegex = /^#?([a-f\d])([a-f\d])([a-f\d])$/i;
      hex = hex.replace(shorthandRegex, function (m, r, g, b) {
        return r + r + g + g + b + b;
      });

      var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
      return result ? {
        r: parseInt(result[1], 16),
        g: parseInt(result[2], 16),
        b: parseInt(result[3], 16)
      } : null;
    };

    ColorUtil.prototype.rgb2hsv = function () {
      var rr,
          gg,
          bb,
          r = arguments[0] / 255,
          g = arguments[1] / 255,
          b = arguments[2] / 255,
          h,
          s,
          v = Math.max(r, g, b),
          diff = v - Math.min(r, g, b),
          diffc = function diffc(c) {
        return (v - c) / 6 / diff + 1 / 2;
      };

      if (diff == 0) {
        h = s = 0;
      } else {
        s = diff / v;
        rr = diffc(r);
        gg = diffc(g);
        bb = diffc(b);

        if (r === v) {
          h = bb - gg;
        } else if (g === v) {
          h = 1 / 3 + rr - bb;
        } else if (b === v) {
          h = 2 / 3 + gg - rr;
        }
        if (h < 0) {
          h += 1;
        } else if (h > 1) {
          h -= 1;
        }
      }
      return {
        h: Math.round(h * 360),
        s: Math.round(s * 100),
        v: Math.round(v * 100)
      };
    };
    ColorUtil.prototype.hsvToRgb = function (h, s, v) {
      var r, g, b;
      var i;
      var f, p, q, t;

      // Make sure our arguments stay in-range
      h = Math.max(0, Math.min(360, h));
      s = Math.max(0, Math.min(100, s));
      v = Math.max(0, Math.min(100, v));

      // We accept saturation and value arguments from 0 to 100 because that's
      // how Photoshop represents those values. Internally, however, the
      // saturation and value are calculated from a range of 0 to 1. We make
      // That conversion here.
      s /= 100;
      v /= 100;

      if (s == 0) {
        // Achromatic (grey)
        r = g = b = v;
        return { r: Math.round(r * 255), g: Math.round(g * 255), b: Math.round(b * 255) };
      }

      h /= 60; // sector 0 to 5
      i = Math.floor(h);
      f = h - i; // factorial part of h
      p = v * (1 - s);
      q = v * (1 - s * f);
      t = v * (1 - s * (1 - f));

      switch (i) {
        case 0:
          r = v;
          g = t;
          b = p;
          break;

        case 1:
          r = q;
          g = v;
          b = p;
          break;

        case 2:
          r = p;
          g = v;
          b = t;
          break;

        case 3:
          r = p;
          g = q;
          b = v;
          break;

        case 4:
          r = t;
          g = p;
          b = v;
          break;

        default:
          // case 5:
          r = v;
          g = p;
          b = q;
      };

      //return [Math.round(r * 255), Math.round(g * 255), Math.round(b * 255)];
      return {
        r: Math.round(r * 255),
        g: Math.round(g * 255),
        b: Math.round(b * 255)
      };
    };

    module.exports = ColorUtil;
  }, {}], 3: [function (require, module, exports) {
    !function ($) {

      "use strict";

      var FOUNDATION_VERSION = '6.2.1';

      // Global Foundation object
      // This is attached to the window, or used as a module for AMD/Browserify
      var Foundation = {
        version: FOUNDATION_VERSION,

        /**
         * Stores initialized plugins.
         */
        _plugins: {},

        /**
         * Stores generated unique ids for plugin instances
         */
        _uuids: [],

        /**
         * Returns a boolean for RTL support
         */
        rtl: function rtl() {
          return $('html').attr('dir') === 'rtl';
        },
        /**
         * Defines a Foundation plugin, adding it to the `Foundation` namespace and the list of plugins to initialize when reflowing.
         * @param {Object} plugin - The constructor of the plugin.
         */
        plugin: function plugin(_plugin, name) {
          // Object key to use when adding to global Foundation object
          // Examples: Foundation.Reveal, Foundation.OffCanvas
          var className = name || functionName(_plugin);
          // Object key to use when storing the plugin, also used to create the identifying data attribute for the plugin
          // Examples: data-reveal, data-off-canvas
          var attrName = hyphenate(className);

          // Add to the Foundation object and the plugins list (for reflowing)
          this._plugins[attrName] = this[className] = _plugin;
        },
        /**
         * @function
         * Populates the _uuids array with pointers to each individual plugin instance.
         * Adds the `zfPlugin` data-attribute to programmatically created plugins to allow use of $(selector).foundation(method) calls.
         * Also fires the initialization event for each plugin, consolidating repeditive code.
         * @param {Object} plugin - an instance of a plugin, usually `this` in context.
         * @param {String} name - the name of the plugin, passed as a camelCased string.
         * @fires Plugin#init
         */
        registerPlugin: function registerPlugin(plugin, name) {
          var pluginName = name ? hyphenate(name) : functionName(plugin.constructor).toLowerCase();
          plugin.uuid = this.GetYoDigits(6, pluginName);

          if (!plugin.$element.attr("data-" + pluginName)) {
            plugin.$element.attr("data-" + pluginName, plugin.uuid);
          }
          if (!plugin.$element.data('zfPlugin')) {
            plugin.$element.data('zfPlugin', plugin);
          }
          /**
           * Fires when the plugin has initialized.
           * @event Plugin#init
           */
          plugin.$element.trigger("init.zf." + pluginName);

          this._uuids.push(plugin.uuid);

          return;
        },
        /**
         * @function
         * Removes the plugins uuid from the _uuids array.
         * Removes the zfPlugin data attribute, as well as the data-plugin-name attribute.
         * Also fires the destroyed event for the plugin, consolidating repeditive code.
         * @param {Object} plugin - an instance of a plugin, usually `this` in context.
         * @fires Plugin#destroyed
         */
        unregisterPlugin: function unregisterPlugin(plugin) {
          var pluginName = hyphenate(functionName(plugin.$element.data('zfPlugin').constructor));

          this._uuids.splice(this._uuids.indexOf(plugin.uuid), 1);
          plugin.$element.removeAttr("data-" + pluginName).removeData('zfPlugin')
          /**
           * Fires when the plugin has been destroyed.
           * @event Plugin#destroyed
           */
          .trigger("destroyed.zf." + pluginName);
          for (var prop in plugin) {
            plugin[prop] = null; //clean up script to prep for garbage collection.
          }
          return;
        },

        /**
         * @function
         * Causes one or more active plugins to re-initialize, resetting event listeners, recalculating positions, etc.
         * @param {String} plugins - optional string of an individual plugin key, attained by calling `$(element).data('pluginName')`, or string of a plugin class i.e. `'dropdown'`
         * @default If no argument is passed, reflow all currently active plugins.
         */
        reInit: function reInit(plugins) {
          var isJQ = plugins instanceof $;
          try {
            if (isJQ) {
              plugins.each(function () {
                $(this).data('zfPlugin')._init();
              });
            } else {
              var type = typeof plugins === "undefined" ? "undefined" : _typeof(plugins),
                  _this = this,
                  fns = {
                'object': function object(plgs) {
                  plgs.forEach(function (p) {
                    p = hyphenate(p);
                    $('[data-' + p + ']').foundation('_init');
                  });
                },
                'string': function string() {
                  plugins = hyphenate(plugins);
                  $('[data-' + plugins + ']').foundation('_init');
                },
                'undefined': function undefined() {
                  this['object'](Object.keys(_this._plugins));
                }
              };
              fns[type](plugins);
            }
          } catch (err) {
            console.error(err);
          } finally {
            return plugins;
          }
        },

        /**
         * returns a random base-36 uid with namespacing
         * @function
         * @param {Number} length - number of random base-36 digits desired. Increase for more random strings.
         * @param {String} namespace - name of plugin to be incorporated in uid, optional.
         * @default {String} '' - if no plugin name is provided, nothing is appended to the uid.
         * @returns {String} - unique id
         */
        GetYoDigits: function GetYoDigits(length, namespace) {
          length = length || 6;
          return Math.round(Math.pow(36, length + 1) - Math.random() * Math.pow(36, length)).toString(36).slice(1) + (namespace ? "-" + namespace : '');
        },
        /**
         * Initialize plugins on any elements within `elem` (and `elem` itself) that aren't already initialized.
         * @param {Object} elem - jQuery object containing the element to check inside. Also checks the element itself, unless it's the `document` object.
         * @param {String|Array} plugins - A list of plugins to initialize. Leave this out to initialize everything.
         */
        reflow: function reflow(elem, plugins) {

          // If plugins is undefined, just grab everything
          if (typeof plugins === 'undefined') {
            plugins = Object.keys(this._plugins);
          }
          // If plugins is a string, convert it to an array with one item
          else if (typeof plugins === 'string') {
              plugins = [plugins];
            }

          var _this = this;

          // Iterate through each plugin
          $.each(plugins, function (i, name) {
            // Get the current plugin
            var plugin = _this._plugins[name];

            // Localize the search to all elements inside elem, as well as elem itself, unless elem === document
            var $elem = $(elem).find('[data-' + name + ']').addBack('[data-' + name + ']');

            // For each plugin found, initialize it
            $elem.each(function () {
              var $el = $(this),
                  opts = {};
              // Don't double-dip on plugins
              if ($el.data('zfPlugin')) {
                console.warn("Tried to initialize " + name + " on an element that already has a Foundation plugin.");
                return;
              }

              if ($el.attr('data-options')) {
                var thing = $el.attr('data-options').split(';').forEach(function (e, i) {
                  var opt = e.split(':').map(function (el) {
                    return el.trim();
                  });
                  if (opt[0]) opts[opt[0]] = parseValue(opt[1]);
                });
              }
              try {
                $el.data('zfPlugin', new plugin($(this), opts));
              } catch (er) {
                console.error(er);
              } finally {
                return;
              }
            });
          });
        },
        getFnName: functionName,
        transitionend: function transitionend($elem) {
          var transitions = {
            'transition': 'transitionend',
            'WebkitTransition': 'webkitTransitionEnd',
            'MozTransition': 'transitionend',
            'OTransition': 'otransitionend'
          };
          var elem = document.createElement('div'),
              end;

          for (var t in transitions) {
            if (typeof elem.style[t] !== 'undefined') {
              end = transitions[t];
            }
          }
          if (end) {
            return end;
          } else {
            end = setTimeout(function () {
              $elem.triggerHandler('transitionend', [$elem]);
            }, 1);
            return 'transitionend';
          }
        }
      };

      Foundation.util = {
        /**
         * Function for applying a debounce effect to a function call.
         * @function
         * @param {Function} func - Function to be called at end of timeout.
         * @param {Number} delay - Time in ms to delay the call of `func`.
         * @returns function
         */
        throttle: function throttle(func, delay) {
          var timer = null;

          return function () {
            var context = this,
                args = arguments;

            if (timer === null) {
              timer = setTimeout(function () {
                func.apply(context, args);
                timer = null;
              }, delay);
            }
          };
        }
      };

      // TODO: consider not making this a jQuery function
      // TODO: need way to reflow vs. re-initialize
      /**
       * The Foundation jQuery method.
       * @param {String|Array} method - An action to perform on the current jQuery object.
       */
      var foundation = function foundation(method) {
        var type = typeof method === "undefined" ? "undefined" : _typeof(method),
            $meta = $('meta.foundation-mq'),
            $noJS = $('.no-js');

        if (!$meta.length) {
          $('<meta class="foundation-mq">').appendTo(document.head);
        }
        if ($noJS.length) {
          $noJS.removeClass('no-js');
        }

        if (type === 'undefined') {
          //needs to initialize the Foundation object, or an individual plugin.
          Foundation.MediaQuery._init();
          Foundation.reflow(this);
        } else if (type === 'string') {
          //an individual method to invoke on a plugin or group of plugins
          var args = Array.prototype.slice.call(arguments, 1); //collect all the arguments, if necessary
          var plugClass = this.data('zfPlugin'); //determine the class of plugin

          if (plugClass !== undefined && plugClass[method] !== undefined) {
            //make sure both the class and method exist
            if (this.length === 1) {
              //if there's only one, call it directly.
              plugClass[method].apply(plugClass, args);
            } else {
              this.each(function (i, el) {
                //otherwise loop through the jQuery collection and invoke the method on each
                plugClass[method].apply($(el).data('zfPlugin'), args);
              });
            }
          } else {
            //error for no class or no method
            throw new ReferenceError("We're sorry, '" + method + "' is not an available method for " + (plugClass ? functionName(plugClass) : 'this element') + '.');
          }
        } else {
          //error for invalid argument type
          throw new TypeError("We're sorry, " + type + " is not a valid parameter. You must use a string representing the method you wish to invoke.");
        }
        return this;
      };

      window.Foundation = Foundation;
      $.fn.foundation = foundation;

      // Polyfill for requestAnimationFrame
      (function () {
        if (!Date.now || !window.Date.now) window.Date.now = Date.now = function () {
          return new Date().getTime();
        };

        var vendors = ['webkit', 'moz'];
        for (var i = 0; i < vendors.length && !window.requestAnimationFrame; ++i) {
          var vp = vendors[i];
          window.requestAnimationFrame = window[vp + 'RequestAnimationFrame'];
          window.cancelAnimationFrame = window[vp + 'CancelAnimationFrame'] || window[vp + 'CancelRequestAnimationFrame'];
        }
        if (/iP(ad|hone|od).*OS 6/.test(window.navigator.userAgent) || !window.requestAnimationFrame || !window.cancelAnimationFrame) {
          var lastTime = 0;
          window.requestAnimationFrame = function (callback) {
            var now = Date.now();
            var nextTime = Math.max(lastTime + 16, now);
            return setTimeout(function () {
              callback(lastTime = nextTime);
            }, nextTime - now);
          };
          window.cancelAnimationFrame = clearTimeout;
        }
        /**
         * Polyfill for performance.now, required by rAF
         */
        if (!window.performance || !window.performance.now) {
          window.performance = {
            start: Date.now(),
            now: function now() {
              return Date.now() - this.start;
            }
          };
        }
      })();
      if (!Function.prototype.bind) {
        Function.prototype.bind = function (oThis) {
          if (typeof this !== 'function') {
            // closest thing possible to the ECMAScript 5
            // internal IsCallable function
            throw new TypeError('Function.prototype.bind - what is trying to be bound is not callable');
          }

          var aArgs = Array.prototype.slice.call(arguments, 1),
              fToBind = this,
              fNOP = function fNOP() {},
              fBound = function fBound() {
            return fToBind.apply(this instanceof fNOP ? this : oThis, aArgs.concat(Array.prototype.slice.call(arguments)));
          };

          if (this.prototype) {
            // native functions don't have a prototype
            fNOP.prototype = this.prototype;
          }
          fBound.prototype = new fNOP();

          return fBound;
        };
      }
      // Polyfill to get the name of a function in IE9
      function functionName(fn) {
        if (Function.prototype.name === undefined) {
          var funcNameRegex = /function\s([^(]{1,})\(/;
          var results = funcNameRegex.exec(fn.toString());
          return results && results.length > 1 ? results[1].trim() : "";
        } else if (fn.prototype === undefined) {
          return fn.constructor.name;
        } else {
          return fn.prototype.constructor.name;
        }
      }
      function parseValue(str) {
        if (/true/.test(str)) return true;else if (/false/.test(str)) return false;else if (!isNaN(str * 1)) return parseFloat(str);
        return str;
      }
      // Convert PascalCase to kebab-case
      // Thank you: http://stackoverflow.com/a/8955580
      function hyphenate(str) {
        return str.replace(/([a-z])([A-Z])/g, '$1-$2').toLowerCase();
      }
    }(jQuery);
  }, {}], 4: [function (require, module, exports) {
    'use strict';

    !function ($) {

      /**
       * Slider module.
       * @module foundation.slider
       * @requires foundation.util.motion
       * @requires foundation.util.triggers
       * @requires foundation.util.keyboard
       * @requires foundation.util.touch
       */

      var Slider = function () {
        /**
         * Creates a new instance of a drilldown menu.
         * @class
         * @param {jQuery} element - jQuery object to make into an accordion menu.
         * @param {Object} options - Overrides to the default plugin settings.
         */

        function Slider(element, options) {
          _classCallCheck(this, Slider);

          this.$element = element;
          this.options = $.extend({}, Slider.defaults, this.$element.data(), options);

          this._init();

          Foundation.registerPlugin(this, 'Slider');
          Foundation.Keyboard.register('Slider', {
            'ltr': {
              'ARROW_RIGHT': 'increase',
              'ARROW_UP': 'increase',
              'ARROW_DOWN': 'decrease',
              'ARROW_LEFT': 'decrease',
              'SHIFT_ARROW_RIGHT': 'increase_fast',
              'SHIFT_ARROW_UP': 'increase_fast',
              'SHIFT_ARROW_DOWN': 'decrease_fast',
              'SHIFT_ARROW_LEFT': 'decrease_fast'
            },
            'rtl': {
              'ARROW_LEFT': 'increase',
              'ARROW_RIGHT': 'decrease',
              'SHIFT_ARROW_LEFT': 'increase_fast',
              'SHIFT_ARROW_RIGHT': 'decrease_fast'
            }
          });
        }

        /**
         * Initilizes the plugin by reading/setting attributes, creating collections and setting the initial position of the handle(s).
         * @function
         * @private
         */


        _createClass(Slider, [{
          key: "_init",
          value: function _init() {
            this.inputs = this.$element.find('input');
            this.handles = this.$element.find('[data-slider-handle]');

            this.$handle = this.handles.eq(0);
            this.$input = this.inputs.length ? this.inputs.eq(0) : $("#" + this.$handle.attr('aria-controls'));
            this.$fill = this.$element.find('[data-slider-fill]').css(this.options.vertical ? 'height' : 'width', 0);

            var isDbl = false,
                _this = this;
            if (this.options.disabled || this.$element.hasClass(this.options.disabledClass)) {
              this.options.disabled = true;
              this.$element.addClass(this.options.disabledClass);
            }
            if (!this.inputs.length) {
              this.inputs = $().add(this.$input);
              this.options.binding = true;
            }
            this._setInitAttr(0);
            this._events(this.$handle);

            if (this.handles[1]) {
              this.options.doubleSided = true;
              this.$handle2 = this.handles.eq(1);
              this.$input2 = this.inputs.length > 1 ? this.inputs.eq(1) : $("#" + this.$handle2.attr('aria-controls'));

              if (!this.inputs[1]) {
                this.inputs = this.inputs.add(this.$input2);
              }
              isDbl = true;

              this._setHandlePos(this.$handle, this.options.initialStart, true, function () {

                _this._setHandlePos(_this.$handle2, _this.options.initialEnd, true);
              });
              // this.$handle.triggerHandler('click.zf.slider');
              this._setInitAttr(1);
              this._events(this.$handle2);
            }

            if (!isDbl) {
              this._setHandlePos(this.$handle, this.options.initialStart, true);
            }
          }

          /**
           * Sets the position of the selected handle and fill bar.
           * @function
           * @private
           * @param {jQuery} $hndl - the selected handle to move.
           * @param {Number} location - floating point between the start and end values of the slider bar.
           * @param {Function} cb - callback function to fire on completion.
           * @fires Slider#moved
           * @fires Slider#changed
           */

        }, {
          key: "_setHandlePos",
          value: function _setHandlePos($hndl, location, noInvert, cb) {
            //might need to alter that slightly for bars that will have odd number selections.
            location = parseFloat(location); //on input change events, convert string to number...grumble.

            // prevent slider from running out of bounds, if value exceeds the limits set through options, override the value to min/max
            if (location < this.options.start) {
              location = this.options.start;
            } else if (location > this.options.end) {
              location = this.options.end;
            }

            var isDbl = this.options.doubleSided;

            if (isDbl) {
              //this block is to prevent 2 handles from crossing eachother. Could/should be improved.
              if (this.handles.index($hndl) === 0) {
                var h2Val = parseFloat(this.$handle2.attr('aria-valuenow'));
                location = location >= h2Val ? h2Val - this.options.step : location;
              } else {
                var h1Val = parseFloat(this.$handle.attr('aria-valuenow'));
                location = location <= h1Val ? h1Val + this.options.step : location;
              }
            }

            //this is for single-handled vertical sliders, it adjusts the value to account for the slider being "upside-down"
            //for click and drag events, it's weird due to the scale(-1, 1) css property
            if (this.options.vertical && !noInvert) {
              location = this.options.end - location;
            }

            var _this = this,
                vert = this.options.vertical,
                hOrW = vert ? 'height' : 'width',
                lOrT = vert ? 'top' : 'left',
                handleDim = $hndl[0].getBoundingClientRect()[hOrW],
                elemDim = this.$element[0].getBoundingClientRect()[hOrW],

            //percentage of bar min/max value based on click or drag point
            pctOfBar = percent(location - this.options.start, this.options.end - this.options.start).toFixed(2),

            //number of actual pixels to shift the handle, based on the percentage obtained above
            pxToMove = (elemDim - handleDim) * pctOfBar,

            //percentage of bar to shift the handle
            movement = (percent(pxToMove, elemDim) * 100).toFixed(this.options.decimal);
            //fixing the decimal value for the location number, is passed to other methods as a fixed floating-point value
            location = parseFloat(location.toFixed(this.options.decimal));
            // declare empty object for css adjustments, only used with 2 handled-sliders
            var css = {};

            this._setValues($hndl, location);

            // TODO update to calculate based on values set to respective inputs??
            if (isDbl) {
              var isLeftHndl = this.handles.index($hndl) === 0,

              //empty variable, will be used for min-height/width for fill bar
              dim,

              //percentage w/h of the handle compared to the slider bar
              handlePct = ~ ~(percent(handleDim, elemDim) * 100);
              //if left handle, the math is slightly different than if it's the right handle, and the left/top property needs to be changed for the fill bar
              if (isLeftHndl) {
                //left or top percentage value to apply to the fill bar.
                css[lOrT] = movement + "%";
                //calculate the new min-height/width for the fill bar.
                dim = parseFloat(this.$handle2[0].style[lOrT]) - movement + handlePct;
                //this callback is necessary to prevent errors and allow the proper placement and initialization of a 2-handled slider
                //plus, it means we don't care if 'dim' isNaN on init, it won't be in the future.
                if (cb && typeof cb === 'function') {
                  cb();
                } //this is only needed for the initialization of 2 handled sliders
              } else {
                  //just caching the value of the left/bottom handle's left/top property
                  var handlePos = parseFloat(this.$handle[0].style[lOrT]);
                  //calculate the new min-height/width for the fill bar. Use isNaN to prevent false positives for numbers <= 0
                  //based on the percentage of movement of the handle being manipulated, less the opposing handle's left/top position, plus the percentage w/h of the handle itself
                  dim = movement - (isNaN(handlePos) ? this.options.initialStart / ((this.options.end - this.options.start) / 100) : handlePos) + handlePct;
                }
              // assign the min-height/width to our css object
              css["min-" + hOrW] = dim + "%";
            }

            this.$element.one('finished.zf.animate', function () {
              /**
               * Fires when the handle is done moving.
               * @event Slider#moved
               */
              _this.$element.trigger('moved.zf.slider', [$hndl]);
            });

            //because we don't know exactly how the handle will be moved, check the amount of time it should take to move.
            var moveTime = this.$element.data('dragging') ? 1000 / 60 : this.options.moveTime;

            Foundation.Move(moveTime, $hndl, function () {
              //adjusting the left/top property of the handle, based on the percentage calculated above
              $hndl.css(lOrT, movement + "%");

              if (!_this.options.doubleSided) {
                //if single-handled, a simple method to expand the fill bar
                _this.$fill.css(hOrW, pctOfBar * 100 + "%");
              } else {
                //otherwise, use the css object we created above
                _this.$fill.css(css);
              }
            });

            /**
             * Fires when the value has not been change for a given time.
             * @event Slider#changed
             */
            clearTimeout(_this.timeout);
            _this.timeout = setTimeout(function () {
              _this.$element.trigger('changed.zf.slider', [$hndl]);
            }, _this.options.changedDelay);
          }

          /**
           * Sets the initial attribute for the slider element.
           * @function
           * @private
           * @param {Number} idx - index of the current handle/input to use.
           */

        }, {
          key: "_setInitAttr",
          value: function _setInitAttr(idx) {
            var id = this.inputs.eq(idx).attr('id') || Foundation.GetYoDigits(6, 'slider');
            this.inputs.eq(idx).attr({
              'id': id,
              'max': this.options.end,
              'min': this.options.start,
              'step': this.options.step
            });
            this.handles.eq(idx).attr({
              'role': 'slider',
              'aria-controls': id,
              'aria-valuemax': this.options.end,
              'aria-valuemin': this.options.start,
              'aria-valuenow': idx === 0 ? this.options.initialStart : this.options.initialEnd,
              'aria-orientation': this.options.vertical ? 'vertical' : 'horizontal',
              'tabindex': 0
            });
          }

          /**
           * Sets the input and `aria-valuenow` values for the slider element.
           * @function
           * @private
           * @param {jQuery} $handle - the currently selected handle.
           * @param {Number} val - floating point of the new value.
           */

        }, {
          key: "_setValues",
          value: function _setValues($handle, val) {
            var idx = this.options.doubleSided ? this.handles.index($handle) : 0;
            this.inputs.eq(idx).val(val);
            $handle.attr('aria-valuenow', val);
          }

          /**
           * Handles events on the slider element.
           * Calculates the new location of the current handle.
           * If there are two handles and the bar was clicked, it determines which handle to move.
           * @function
           * @private
           * @param {Object} e - the `event` object passed from the listener.
           * @param {jQuery} $handle - the current handle to calculate for, if selected.
           * @param {Number} val - floating point number for the new value of the slider.
           * TODO clean this up, there's a lot of repeated code between this and the _setHandlePos fn.
           */

        }, {
          key: "_handleEvent",
          value: function _handleEvent(e, $handle, val) {
            var value, hasVal;
            if (!val) {
              //click or drag events
              e.preventDefault();
              var _this = this,
                  vertical = this.options.vertical,
                  param = vertical ? 'height' : 'width',
                  direction = vertical ? 'top' : 'left',
                  pageXY = vertical ? e.pageY : e.pageX,
                  halfOfHandle = this.$handle[0].getBoundingClientRect()[param] / 2,
                  barDim = this.$element[0].getBoundingClientRect()[param],
                  barOffset = this.$element.offset()[direction] - pageXY,

              //if the cursor position is less than or greater than the elements bounding coordinates, set coordinates within those bounds
              barXY = barOffset > 0 ? -halfOfHandle : barOffset - halfOfHandle < -barDim ? barDim : Math.abs(barOffset),
                  offsetPct = percent(barXY, barDim);
              value = (this.options.end - this.options.start) * offsetPct + this.options.start;

              // turn everything around for RTL, yay math!
              if (Foundation.rtl() && !this.options.vertical) {
                value = this.options.end - value;
              }

              value = _this._adjustValue(null, value);
              //boolean flag for the setHandlePos fn, specifically for vertical sliders
              hasVal = false;

              if (!$handle) {
                //figure out which handle it is, pass it to the next function.
                var firstHndlPos = absPosition(this.$handle, direction, barXY, param),
                    secndHndlPos = absPosition(this.$handle2, direction, barXY, param);
                $handle = firstHndlPos <= secndHndlPos ? this.$handle : this.$handle2;
              }
            } else {
              //change event on input
              value = this._adjustValue(null, val);
              hasVal = true;
            }

            this._setHandlePos($handle, value, hasVal);
          }

          /**
           * Adjustes value for handle in regard to step value. returns adjusted value
           * @function
           * @private
           * @param {jQuery} $handle - the selected handle.
           * @param {Number} value - value to adjust. used if $handle is falsy
           */

        }, {
          key: "_adjustValue",
          value: function _adjustValue($handle, value) {
            var val,
                step = this.options.step,
                div = parseFloat(step / 2),
                left,
                prev_val,
                next_val;
            if (!!$handle) {
              val = parseFloat($handle.attr('aria-valuenow'));
            } else {
              val = value;
            }
            left = val % step;
            prev_val = val - left;
            next_val = prev_val + step;
            if (left === 0) {
              return val;
            }
            val = val >= prev_val + div ? next_val : prev_val;
            return val;
          }

          /**
           * Adds event listeners to the slider elements.
           * @function
           * @private
           * @param {jQuery} $handle - the current handle to apply listeners to.
           */

        }, {
          key: "_events",
          value: function _events($handle) {
            if (this.options.disabled) {
              return false;
            }

            var _this = this,
                curHandle,
                timer;

            this.inputs.off('change.zf.slider').on('change.zf.slider', function (e) {
              var idx = _this.inputs.index($(this));
              _this._handleEvent(e, _this.handles.eq(idx), $(this).val());
            });

            if (this.options.clickSelect) {
              this.$element.off('click.zf.slider').on('click.zf.slider', function (e) {
                if (_this.$element.data('dragging')) {
                  return false;
                }

                if (!$(e.target).is('[data-slider-handle]')) {
                  if (_this.options.doubleSided) {
                    _this._handleEvent(e);
                  } else {
                    _this._handleEvent(e, _this.$handle);
                  }
                }
              });
            }

            if (this.options.draggable) {
              this.handles.addTouch();

              var $body = $('body');
              $handle.off('mousedown.zf.slider').on('mousedown.zf.slider', function (e) {
                $handle.addClass('is-dragging');
                _this.$fill.addClass('is-dragging'); //
                _this.$element.data('dragging', true);

                curHandle = $(e.currentTarget);

                $body.on('mousemove.zf.slider', function (e) {
                  e.preventDefault();

                  _this._handleEvent(e, curHandle);
                }).on('mouseup.zf.slider', function (e) {
                  _this._handleEvent(e, curHandle);

                  $handle.removeClass('is-dragging');
                  _this.$fill.removeClass('is-dragging');
                  _this.$element.data('dragging', false);

                  $body.off('mousemove.zf.slider mouseup.zf.slider');
                });
              });
            }

            $handle.off('keydown.zf.slider').on('keydown.zf.slider', function (e) {
              var _$handle = $(this),
                  idx = _this.options.doubleSided ? _this.handles.index(_$handle) : 0,
                  oldValue = parseFloat(_this.inputs.eq(idx).val()),
                  newValue;

              // handle keyboard event with keyboard util
              Foundation.Keyboard.handleKey(e, 'Slider', {
                decrease: function decrease() {
                  newValue = oldValue - _this.options.step;
                },
                increase: function increase() {
                  newValue = oldValue + _this.options.step;
                },
                decrease_fast: function decrease_fast() {
                  newValue = oldValue - _this.options.step * 10;
                },
                increase_fast: function increase_fast() {
                  newValue = oldValue + _this.options.step * 10;
                },
                handled: function handled() {
                  // only set handle pos when event was handled specially
                  e.preventDefault();
                  _this._setHandlePos(_$handle, newValue, true);
                }
              });
              /*if (newValue) { // if pressed key has special function, update value
                e.preventDefault();
                _this._setHandlePos(_$handle, newValue);
              }*/
            });
          }

          /**
           * Destroys the slider plugin.
           */

        }, {
          key: "destroy",
          value: function destroy() {
            this.handles.off('.zf.slider');
            this.inputs.off('.zf.slider');
            this.$element.off('.zf.slider');

            Foundation.unregisterPlugin(this);
          }
        }]);

        return Slider;
      }();

      Slider.defaults = {
        /**
         * Minimum value for the slider scale.
         * @option
         * @example 0
         */
        start: 0,
        /**
         * Maximum value for the slider scale.
         * @option
         * @example 100
         */
        end: 100,
        /**
         * Minimum value change per change event.
         * @option
         * @example 1
         */
        step: 1,
        /**
         * Value at which the handle/input *(left handle/first input)* should be set to on initialization.
         * @option
         * @example 0
         */
        initialStart: 0,
        /**
         * Value at which the right handle/second input should be set to on initialization.
         * @option
         * @example 100
         */
        initialEnd: 100,
        /**
         * Allows the input to be located outside the container and visible. Set to by the JS
         * @option
         * @example false
         */
        binding: false,
        /**
         * Allows the user to click/tap on the slider bar to select a value.
         * @option
         * @example true
         */
        clickSelect: true,
        /**
         * Set to true and use the `vertical` class to change alignment to vertical.
         * @option
         * @example false
         */
        vertical: false,
        /**
         * Allows the user to drag the slider handle(s) to select a value.
         * @option
         * @example true
         */
        draggable: true,
        /**
         * Disables the slider and prevents event listeners from being applied. Double checked by JS with `disabledClass`.
         * @option
         * @example false
         */
        disabled: false,
        /**
         * Allows the use of two handles. Double checked by the JS. Changes some logic handling.
         * @option
         * @example false
         */
        doubleSided: false,
        /**
         * Potential future feature.
         */
        // steps: 100,
        /**
         * Number of decimal places the plugin should go to for floating point precision.
         * @option
         * @example 2
         */
        decimal: 2,
        /**
         * Time delay for dragged elements.
         */
        // dragDelay: 0,
        /**
         * Time, in ms, to animate the movement of a slider handle if user clicks/taps on the bar. Needs to be manually set if updating the transition time in the Sass settings.
         * @option
         * @example 200
         */
        moveTime: 200, //update this if changing the transition time in the sass
        /**
         * Class applied to disabled sliders.
         * @option
         * @example 'disabled'
         */
        disabledClass: 'disabled',
        /**
         * Will invert the default layout for a vertical<span data-tooltip title="who would do this???"> </span>slider.
         * @option
         * @example false
         */
        invertVertical: false,
        /**
         * Milliseconds before the `changed.zf-slider` event is triggered after value change. 
         * @option
         * @example 500
         */
        changedDelay: 500
      };

      function percent(frac, num) {
        return frac / num;
      }
      function absPosition($handle, dir, clickPos, param) {
        return Math.abs($handle.position()[dir] + $handle[param]() / 2 - clickPos);
      }

      // Window exports
      Foundation.plugin(Slider, 'Slider');
    }(jQuery);

    //*********this is in case we go to static, absolute positions instead of dynamic positioning********
    // this.setSteps(function() {
    //   _this._events();
    //   var initStart = _this.options.positions[_this.options.initialStart - 1] || null;
    //   var initEnd = _this.options.initialEnd ? _this.options.position[_this.options.initialEnd - 1] : null;
    //   if (initStart || initEnd) {
    //     _this._handleEvent(initStart, initEnd);
    //   }
    // });

    //***********the other part of absolute positions*************
    // Slider.prototype.setSteps = function(cb) {
    //   var posChange = this.$element.outerWidth() / this.options.steps;
    //   var counter = 0
    //   while(counter < this.options.steps) {
    //     if (counter) {
    //       this.options.positions.push(this.options.positions[counter - 1] + posChange);
    //     } else {
    //       this.options.positions.push(posChange);
    //     }
    //     counter++;
    //   }
    //   cb();
    // };
  }, {}], 5: [function (require, module, exports) {
    /*******************************************
     *                                         *
     * This util was created by Marius Olbertz *
     * Please thank Marius on GitHub /owlbertz *
     * or the web http://www.mariusolbertz.de/ *
     *                                         *
     ******************************************/

    'use strict';

    !function ($) {

      var keyCodes = {
        9: 'TAB',
        13: 'ENTER',
        27: 'ESCAPE',
        32: 'SPACE',
        37: 'ARROW_LEFT',
        38: 'ARROW_UP',
        39: 'ARROW_RIGHT',
        40: 'ARROW_DOWN'
      };

      var commands = {};

      var Keyboard = {
        keys: getKeyCodes(keyCodes),

        /**
         * Parses the (keyboard) event and returns a String that represents its key
         * Can be used like Foundation.parseKey(event) === Foundation.keys.SPACE
         * @param {Event} event - the event generated by the event handler
         * @return String key - String that represents the key pressed
         */
        parseKey: function parseKey(event) {
          var key = keyCodes[event.which || event.keyCode] || String.fromCharCode(event.which).toUpperCase();
          if (event.shiftKey) key = "SHIFT_" + key;
          if (event.ctrlKey) key = "CTRL_" + key;
          if (event.altKey) key = "ALT_" + key;
          return key;
        },


        /**
         * Handles the given (keyboard) event
         * @param {Event} event - the event generated by the event handler
         * @param {String} component - Foundation component's name, e.g. Slider or Reveal
         * @param {Objects} functions - collection of functions that are to be executed
         */
        handleKey: function handleKey(event, component, functions) {
          var commandList = commands[component],
              keyCode = this.parseKey(event),
              cmds,
              command,
              fn;

          if (!commandList) return console.warn('Component not defined!');

          if (typeof commandList.ltr === 'undefined') {
            // this component does not differentiate between ltr and rtl
            cmds = commandList; // use plain list
          } else {
              // merge ltr and rtl: if document is rtl, rtl overwrites ltr and vice versa
              if (Foundation.rtl()) cmds = $.extend({}, commandList.ltr, commandList.rtl);else cmds = $.extend({}, commandList.rtl, commandList.ltr);
            }
          command = cmds[keyCode];

          fn = functions[command];
          if (fn && typeof fn === 'function') {
            // execute function  if exists
            fn.apply();
            if (functions.handled || typeof functions.handled === 'function') {
              // execute function when event was handled
              functions.handled.apply();
            }
          } else {
            if (functions.unhandled || typeof functions.unhandled === 'function') {
              // execute function when event was not handled
              functions.unhandled.apply();
            }
          }
        },


        /**
         * Finds all focusable elements within the given `$element`
         * @param {jQuery} $element - jQuery object to search within
         * @return {jQuery} $focusable - all focusable elements within `$element`
         */
        findFocusable: function findFocusable($element) {
          return $element.find('a[href], area[href], input:not([disabled]), select:not([disabled]), textarea:not([disabled]), button:not([disabled]), iframe, object, embed, *[tabindex], *[contenteditable]').filter(function () {
            if (!$(this).is(':visible') || $(this).attr('tabindex') < 0) {
              return false;
            } //only have visible elements and those that have a tabindex greater or equal 0
            return true;
          });
        },


        /**
         * Returns the component name name
         * @param {Object} component - Foundation component, e.g. Slider or Reveal
         * @return String componentName
         */

        register: function register(componentName, cmds) {
          commands[componentName] = cmds;
        }
      };

      /*
       * Constants for easier comparing.
       * Can be used like Foundation.parseKey(event) === Foundation.keys.SPACE
       */
      function getKeyCodes(kcs) {
        var k = {};
        for (var kc in kcs) {
          k[kcs[kc]] = kcs[kc];
        }return k;
      }

      Foundation.Keyboard = Keyboard;
    }(jQuery);
  }, {}], 6: [function (require, module, exports) {
    'use strict';

    !function ($) {

      // Default set of media queries
      var defaultQueries = {
        'default': 'only screen',
        landscape: 'only screen and (orientation: landscape)',
        portrait: 'only screen and (orientation: portrait)',
        retina: 'only screen and (-webkit-min-device-pixel-ratio: 2),' + 'only screen and (min--moz-device-pixel-ratio: 2),' + 'only screen and (-o-min-device-pixel-ratio: 2/1),' + 'only screen and (min-device-pixel-ratio: 2),' + 'only screen and (min-resolution: 192dpi),' + 'only screen and (min-resolution: 2dppx)'
      };

      var MediaQuery = {
        queries: [],

        current: '',

        /**
         * Initializes the media query helper, by extracting the breakpoint list from the CSS and activating the breakpoint watcher.
         * @function
         * @private
         */
        _init: function _init() {
          var self = this;
          var extractedStyles = $('.foundation-mq').css('font-family');
          var namedQueries;

          namedQueries = parseStyleToObject(extractedStyles);

          for (var key in namedQueries) {
            self.queries.push({
              name: key,
              value: "only screen and (min-width: " + namedQueries[key] + ")"
            });
          }

          this.current = this._getCurrentSize();

          this._watcher();
        },


        /**
         * Checks if the screen is at least as wide as a breakpoint.
         * @function
         * @param {String} size - Name of the breakpoint to check.
         * @returns {Boolean} `true` if the breakpoint matches, `false` if it's smaller.
         */
        atLeast: function atLeast(size) {
          var query = this.get(size);

          if (query) {
            return window.matchMedia(query).matches;
          }

          return false;
        },


        /**
         * Gets the media query of a breakpoint.
         * @function
         * @param {String} size - Name of the breakpoint to get.
         * @returns {String|null} - The media query of the breakpoint, or `null` if the breakpoint doesn't exist.
         */
        get: function get(size) {
          for (var i in this.queries) {
            var query = this.queries[i];
            if (size === query.name) return query.value;
          }

          return null;
        },


        /**
         * Gets the current breakpoint name by testing every breakpoint and returning the last one to match (the biggest one).
         * @function
         * @private
         * @returns {String} Name of the current breakpoint.
         */
        _getCurrentSize: function _getCurrentSize() {
          var matched;

          for (var i = 0; i < this.queries.length; i++) {
            var query = this.queries[i];

            if (window.matchMedia(query.value).matches) {
              matched = query;
            }
          }

          if ((typeof matched === "undefined" ? "undefined" : _typeof(matched)) === 'object') {
            return matched.name;
          } else {
            return matched;
          }
        },


        /**
         * Activates the breakpoint watcher, which fires an event on the window whenever the breakpoint changes.
         * @function
         * @private
         */
        _watcher: function _watcher() {
          var _this2 = this;

          $(window).on('resize.zf.mediaquery', function () {
            var newSize = _this2._getCurrentSize();

            if (newSize !== _this2.current) {
              // Broadcast the media query change on the window
              $(window).trigger('changed.zf.mediaquery', [newSize, _this2.current]);

              // Change the current media query
              _this2.current = newSize;
            }
          });
        }
      };

      Foundation.MediaQuery = MediaQuery;

      // matchMedia() polyfill - Test a CSS media type/query in JS.
      // Authors & copyright (c) 2012: Scott Jehl, Paul Irish, Nicholas Zakas, David Knight. Dual MIT/BSD license
      window.matchMedia || (window.matchMedia = function () {
        'use strict';

        // For browsers that support matchMedium api such as IE 9 and webkit

        var styleMedia = window.styleMedia || window.media;

        // For those that don't support matchMedium
        if (!styleMedia) {
          var style = document.createElement('style'),
              script = document.getElementsByTagName('script')[0],
              info = null;

          style.type = 'text/css';
          style.id = 'matchmediajs-test';

          script.parentNode.insertBefore(style, script);

          // 'style.currentStyle' is used by IE <= 8 and 'window.getComputedStyle' for all other browsers
          info = 'getComputedStyle' in window && window.getComputedStyle(style, null) || style.currentStyle;

          styleMedia = {
            matchMedium: function matchMedium(media) {
              var text = "@media " + media + "{ #matchmediajs-test { width: 1px; } }";

              // 'style.styleSheet' is used by IE <= 8 and 'style.textContent' for all other browsers
              if (style.styleSheet) {
                style.styleSheet.cssText = text;
              } else {
                style.textContent = text;
              }

              // Test if media query is true or false
              return info.width === '1px';
            }
          };
        }

        return function (media) {
          return {
            matches: styleMedia.matchMedium(media || 'all'),
            media: media || 'all'
          };
        };
      }());

      // Thank you: https://github.com/sindresorhus/query-string
      function parseStyleToObject(str) {
        var styleObject = {};

        if (typeof str !== 'string') {
          return styleObject;
        }

        str = str.trim().slice(1, -1); // browsers re-quote string style values

        if (!str) {
          return styleObject;
        }

        styleObject = str.split('&').reduce(function (ret, param) {
          var parts = param.replace(/\+/g, ' ').split('=');
          var key = parts[0];
          var val = parts[1];
          key = decodeURIComponent(key);

          // missing `=` should be `null`:
          // http://w3.org/TR/2012/WD-url-20120524/#collect-url-parameters
          val = val === undefined ? null : decodeURIComponent(val);

          if (!ret.hasOwnProperty(key)) {
            ret[key] = val;
          } else if (Array.isArray(ret[key])) {
            ret[key].push(val);
          } else {
            ret[key] = [ret[key], val];
          }
          return ret;
        }, {});

        return styleObject;
      }

      Foundation.MediaQuery = MediaQuery;
    }(jQuery);
  }, {}], 7: [function (require, module, exports) {
    'use strict';

    !function ($) {

      /**
       * Motion module.
       * @module foundation.motion
       */

      var initClasses = ['mui-enter', 'mui-leave'];
      var activeClasses = ['mui-enter-active', 'mui-leave-active'];

      var Motion = {
        animateIn: function animateIn(element, animation, cb) {
          animate(true, element, animation, cb);
        },

        animateOut: function animateOut(element, animation, cb) {
          animate(false, element, animation, cb);
        }
      };

      function Move(duration, elem, fn) {
        var anim,
            prog,
            start = null;
        // console.log('called');

        function move(ts) {
          if (!start) start = window.performance.now();
          // console.log(start, ts);
          prog = ts - start;
          fn.apply(elem);

          if (prog < duration) {
            anim = window.requestAnimationFrame(move, elem);
          } else {
            window.cancelAnimationFrame(anim);
            elem.trigger('finished.zf.animate', [elem]).triggerHandler('finished.zf.animate', [elem]);
          }
        }
        anim = window.requestAnimationFrame(move);
      }

      /**
       * Animates an element in or out using a CSS transition class.
       * @function
       * @private
       * @param {Boolean} isIn - Defines if the animation is in or out.
       * @param {Object} element - jQuery or HTML object to animate.
       * @param {String} animation - CSS class to use.
       * @param {Function} cb - Callback to run when animation is finished.
       */
      function animate(isIn, element, animation, cb) {
        element = $(element).eq(0);

        if (!element.length) return;

        var initClass = isIn ? initClasses[0] : initClasses[1];
        var activeClass = isIn ? activeClasses[0] : activeClasses[1];

        // Set up the animation
        reset();

        element.addClass(animation).css('transition', 'none');

        requestAnimationFrame(function () {
          element.addClass(initClass);
          if (isIn) element.show();
        });

        // Start the animation
        requestAnimationFrame(function () {
          element[0].offsetWidth;
          element.css('transition', '').addClass(activeClass);
        });

        // Clean up the animation when it finishes
        element.one(Foundation.transitionend(element), finish);

        // Hides the element (for out animations), resets the element, and runs a callback
        function finish() {
          if (!isIn) element.hide();
          reset();
          if (cb) cb.apply(element);
        }

        // Resets transitions and removes motion-specific classes
        function reset() {
          element[0].style.transitionDuration = 0;
          element.removeClass(initClass + " " + activeClass + " " + animation);
        }
      }

      Foundation.Move = Move;
      Foundation.Motion = Motion;
    }(jQuery);
  }, {}], 8: [function (require, module, exports) {
    //**************************************************
    //**Work inspired by multiple jquery swipe plugins**
    //**Done by Yohai Ararat ***************************
    //**************************************************
    (function ($) {

      $.spotSwipe = {
        version: '1.0.0',
        enabled: 'ontouchstart' in document.documentElement,
        preventDefault: false,
        moveThreshold: 75,
        timeThreshold: 200
      };

      var startPosX,
          startPosY,
          startTime,
          elapsedTime,
          isMoving = false;

      function onTouchEnd() {
        //  alert(this);
        this.removeEventListener('touchmove', onTouchMove);
        this.removeEventListener('touchend', onTouchEnd);
        isMoving = false;
      }

      function onTouchMove(e) {
        if ($.spotSwipe.preventDefault) {
          e.preventDefault();
        }
        if (isMoving) {
          var x = e.touches[0].pageX;
          var y = e.touches[0].pageY;
          var dx = startPosX - x;
          var dy = startPosY - y;
          var dir;
          elapsedTime = new Date().getTime() - startTime;
          if (Math.abs(dx) >= $.spotSwipe.moveThreshold && elapsedTime <= $.spotSwipe.timeThreshold) {
            dir = dx > 0 ? 'left' : 'right';
          }
          // else if(Math.abs(dy) >= $.spotSwipe.moveThreshold && elapsedTime <= $.spotSwipe.timeThreshold) {
          //   dir = dy > 0 ? 'down' : 'up';
          // }
          if (dir) {
            e.preventDefault();
            onTouchEnd.call(this);
            $(this).trigger('swipe', dir).trigger("swipe" + dir);
          }
        }
      }

      function onTouchStart(e) {
        if (e.touches.length == 1) {
          startPosX = e.touches[0].pageX;
          startPosY = e.touches[0].pageY;
          isMoving = true;
          startTime = new Date().getTime();
          this.addEventListener('touchmove', onTouchMove, false);
          this.addEventListener('touchend', onTouchEnd, false);
        }
      }

      function init() {
        this.addEventListener && this.addEventListener('touchstart', onTouchStart, false);
      }

      function teardown() {
        this.removeEventListener('touchstart', onTouchStart);
      }

      $.event.special.swipe = { setup: init };

      $.each(['left', 'up', 'down', 'right'], function () {
        $.event.special["swipe" + this] = { setup: function setup() {
            $(this).on('swipe', $.noop);
          } };
      });
    })(jQuery);
    /****************************************************
     * Method for adding psuedo drag events to elements *
     ***************************************************/
    !function ($) {
      $.fn.addTouch = function () {
        this.each(function (i, el) {
          $(el).bind('touchstart touchmove touchend touchcancel', function () {
            //we pass the original event object because the jQuery event
            //object is normalized to w3c specs and does not provide the TouchList
            handleTouch(event);
          });
        });

        var handleTouch = function handleTouch(event) {
          var touches = event.changedTouches,
              first = touches[0],
              eventTypes = {
            touchstart: 'mousedown',
            touchmove: 'mousemove',
            touchend: 'mouseup'
          },
              type = eventTypes[event.type],
              simulatedEvent;

          if ('MouseEvent' in window && typeof window.MouseEvent === 'function') {
            simulatedEvent = window.MouseEvent(type, {
              'bubbles': true,
              'cancelable': true,
              'screenX': first.screenX,
              'screenY': first.screenY,
              'clientX': first.clientX,
              'clientY': first.clientY
            });
          } else {
            simulatedEvent = document.createEvent('MouseEvent');
            simulatedEvent.initMouseEvent(type, true, true, window, 1, first.screenX, first.screenY, first.clientX, first.clientY, false, false, false, false, 0 /*left*/, null);
          }
          first.target.dispatchEvent(simulatedEvent);
        };
      };
    }(jQuery);

    //**********************************
    //**From the jQuery Mobile Library**
    //**need to recreate functionality**
    //**and try to improve if possible**
    //**********************************

    /* Removing the jQuery function ****
    ************************************
    
    (function( $, window, undefined ) {
    
    	var $document = $( document ),
    		// supportTouch = $.mobile.support.touch,
    		touchStartEvent = 'touchstart'//supportTouch ? "touchstart" : "mousedown",
    		touchStopEvent = 'touchend'//supportTouch ? "touchend" : "mouseup",
    		touchMoveEvent = 'touchmove'//supportTouch ? "touchmove" : "mousemove";
    
    	// setup new event shortcuts
    	$.each( ( "touchstart touchmove touchend " +
    		"swipe swipeleft swiperight" ).split( " " ), function( i, name ) {
    
    		$.fn[ name ] = function( fn ) {
    			return fn ? this.bind( name, fn ) : this.trigger( name );
    		};
    
    		// jQuery < 1.8
    		if ( $.attrFn ) {
    			$.attrFn[ name ] = true;
    		}
    	});
    
    	function triggerCustomEvent( obj, eventType, event, bubble ) {
    		var originalType = event.type;
    		event.type = eventType;
    		if ( bubble ) {
    			$.event.trigger( event, undefined, obj );
    		} else {
    			$.event.dispatch.call( obj, event );
    		}
    		event.type = originalType;
    	}
    
    	// also handles taphold
    
    	// Also handles swipeleft, swiperight
    	$.event.special.swipe = {
    
    		// More than this horizontal displacement, and we will suppress scrolling.
    		scrollSupressionThreshold: 30,
    
    		// More time than this, and it isn't a swipe.
    		durationThreshold: 1000,
    
    		// Swipe horizontal displacement must be more than this.
    		horizontalDistanceThreshold: window.devicePixelRatio >= 2 ? 15 : 30,
    
    		// Swipe vertical displacement must be less than this.
    		verticalDistanceThreshold: window.devicePixelRatio >= 2 ? 15 : 30,
    
    		getLocation: function ( event ) {
    			var winPageX = window.pageXOffset,
    				winPageY = window.pageYOffset,
    				x = event.clientX,
    				y = event.clientY;
    
    			if ( event.pageY === 0 && Math.floor( y ) > Math.floor( event.pageY ) ||
    				event.pageX === 0 && Math.floor( x ) > Math.floor( event.pageX ) ) {
    
    				// iOS4 clientX/clientY have the value that should have been
    				// in pageX/pageY. While pageX/page/ have the value 0
    				x = x - winPageX;
    				y = y - winPageY;
    			} else if ( y < ( event.pageY - winPageY) || x < ( event.pageX - winPageX ) ) {
    
    				// Some Android browsers have totally bogus values for clientX/Y
    				// when scrolling/zooming a page. Detectable since clientX/clientY
    				// should never be smaller than pageX/pageY minus page scroll
    				x = event.pageX - winPageX;
    				y = event.pageY - winPageY;
    			}
    
    			return {
    				x: x,
    				y: y
    			};
    		},
    
    		start: function( event ) {
    			var data = event.originalEvent.touches ?
    					event.originalEvent.touches[ 0 ] : event,
    				location = $.event.special.swipe.getLocation( data );
    			return {
    						time: ( new Date() ).getTime(),
    						coords: [ location.x, location.y ],
    						origin: $( event.target )
    					};
    		},
    
    		stop: function( event ) {
    			var data = event.originalEvent.touches ?
    					event.originalEvent.touches[ 0 ] : event,
    				location = $.event.special.swipe.getLocation( data );
    			return {
    						time: ( new Date() ).getTime(),
    						coords: [ location.x, location.y ]
    					};
    		},
    
    		handleSwipe: function( start, stop, thisObject, origTarget ) {
    			if ( stop.time - start.time < $.event.special.swipe.durationThreshold &&
    				Math.abs( start.coords[ 0 ] - stop.coords[ 0 ] ) > $.event.special.swipe.horizontalDistanceThreshold &&
    				Math.abs( start.coords[ 1 ] - stop.coords[ 1 ] ) < $.event.special.swipe.verticalDistanceThreshold ) {
    				var direction = start.coords[0] > stop.coords[ 0 ] ? "swipeleft" : "swiperight";
    
    				triggerCustomEvent( thisObject, "swipe", $.Event( "swipe", { target: origTarget, swipestart: start, swipestop: stop }), true );
    				triggerCustomEvent( thisObject, direction,$.Event( direction, { target: origTarget, swipestart: start, swipestop: stop } ), true );
    				return true;
    			}
    			return false;
    
    		},
    
    		// This serves as a flag to ensure that at most one swipe event event is
    		// in work at any given time
    		eventInProgress: false,
    
    		setup: function() {
    			var events,
    				thisObject = this,
    				$this = $( thisObject ),
    				context = {};
    
    			// Retrieve the events data for this element and add the swipe context
    			events = $.data( this, "mobile-events" );
    			if ( !events ) {
    				events = { length: 0 };
    				$.data( this, "mobile-events", events );
    			}
    			events.length++;
    			events.swipe = context;
    
    			context.start = function( event ) {
    
    				// Bail if we're already working on a swipe event
    				if ( $.event.special.swipe.eventInProgress ) {
    					return;
    				}
    				$.event.special.swipe.eventInProgress = true;
    
    				var stop,
    					start = $.event.special.swipe.start( event ),
    					origTarget = event.target,
    					emitted = false;
    
    				context.move = function( event ) {
    					if ( !start || event.isDefaultPrevented() ) {
    						return;
    					}
    
    					stop = $.event.special.swipe.stop( event );
    					if ( !emitted ) {
    						emitted = $.event.special.swipe.handleSwipe( start, stop, thisObject, origTarget );
    						if ( emitted ) {
    
    							// Reset the context to make way for the next swipe event
    							$.event.special.swipe.eventInProgress = false;
    						}
    					}
    					// prevent scrolling
    					if ( Math.abs( start.coords[ 0 ] - stop.coords[ 0 ] ) > $.event.special.swipe.scrollSupressionThreshold ) {
    						event.preventDefault();
    					}
    				};
    
    				context.stop = function() {
    						emitted = true;
    
    						// Reset the context to make way for the next swipe event
    						$.event.special.swipe.eventInProgress = false;
    						$document.off( touchMoveEvent, context.move );
    						context.move = null;
    				};
    
    				$document.on( touchMoveEvent, context.move )
    					.one( touchStopEvent, context.stop );
    			};
    			$this.on( touchStartEvent, context.start );
    		},
    
    		teardown: function() {
    			var events, context;
    
    			events = $.data( this, "mobile-events" );
    			if ( events ) {
    				context = events.swipe;
    				delete events.swipe;
    				events.length--;
    				if ( events.length === 0 ) {
    					$.removeData( this, "mobile-events" );
    				}
    			}
    
    			if ( context ) {
    				if ( context.start ) {
    					$( this ).off( touchStartEvent, context.start );
    				}
    				if ( context.move ) {
    					$document.off( touchMoveEvent, context.move );
    				}
    				if ( context.stop ) {
    					$document.off( touchStopEvent, context.stop );
    				}
    			}
    		}
    	};
    	$.each({
    		swipeleft: "swipe.left",
    		swiperight: "swipe.right"
    	}, function( event, sourceEvent ) {
    
    		$.event.special[ event ] = {
    			setup: function() {
    				$( this ).bind( sourceEvent, $.noop );
    			},
    			teardown: function() {
    				$( this ).unbind( sourceEvent );
    			}
    		};
    	});
    })( jQuery, this );
    */
  }, {}], 9: [function (require, module, exports) {
    'use strict';

    !function ($) {

      var MutationObserver = function () {
        var prefixes = ['WebKit', 'Moz', 'O', 'Ms', ''];
        for (var i = 0; i < prefixes.length; i++) {
          if (prefixes[i] + "MutationObserver" in window) {
            return window[prefixes[i] + "MutationObserver"];
          }
        }
        return false;
      }();

      var triggers = function triggers(el, type) {
        el.data(type).split(' ').forEach(function (id) {
          $("#" + id)[type === 'close' ? 'trigger' : 'triggerHandler'](type + ".zf.trigger", [el]);
        });
      };
      // Elements with [data-open] will reveal a plugin that supports it when clicked.
      $(document).on('click.zf.trigger', '[data-open]', function () {
        triggers($(this), 'open');
      });

      // Elements with [data-close] will close a plugin that supports it when clicked.
      // If used without a value on [data-close], the event will bubble, allowing it to close a parent component.
      $(document).on('click.zf.trigger', '[data-close]', function () {
        var id = $(this).data('close');
        if (id) {
          triggers($(this), 'close');
        } else {
          $(this).trigger('close.zf.trigger');
        }
      });

      // Elements with [data-toggle] will toggle a plugin that supports it when clicked.
      $(document).on('click.zf.trigger', '[data-toggle]', function () {
        triggers($(this), 'toggle');
      });

      // Elements with [data-closable] will respond to close.zf.trigger events.
      $(document).on('close.zf.trigger', '[data-closable]', function (e) {
        e.stopPropagation();
        var animation = $(this).data('closable');

        if (animation !== '') {
          Foundation.Motion.animateOut($(this), animation, function () {
            $(this).trigger('closed.zf');
          });
        } else {
          $(this).fadeOut().trigger('closed.zf');
        }
      });

      $(document).on('focus.zf.trigger blur.zf.trigger', '[data-toggle-focus]', function () {
        var id = $(this).data('toggle-focus');
        $("#" + id).triggerHandler('toggle.zf.trigger', [$(this)]);
      });

      /**
      * Fires once after all other scripts have loaded
      * @function
      * @private
      */
      $(window).load(function () {
        checkListeners();
      });

      function checkListeners() {
        eventsListener();
        resizeListener();
        scrollListener();
        closemeListener();
      }

      //******** only fires this function once on load, if there's something to watch ********
      function closemeListener(pluginName) {
        var yetiBoxes = $('[data-yeti-box]'),
            plugNames = ['dropdown', 'tooltip', 'reveal'];

        if (pluginName) {
          if (typeof pluginName === 'string') {
            plugNames.push(pluginName);
          } else if ((typeof pluginName === "undefined" ? "undefined" : _typeof(pluginName)) === 'object' && typeof pluginName[0] === 'string') {
            plugNames.concat(pluginName);
          } else {
            console.error('Plugin names must be strings');
          }
        }
        if (yetiBoxes.length) {
          var listeners = plugNames.map(function (name) {
            return "closeme.zf." + name;
          }).join(' ');

          $(window).off(listeners).on(listeners, function (e, pluginId) {
            var plugin = e.namespace.split('.')[0];
            var plugins = $("[data-" + plugin + "]").not("[data-yeti-box=\"" + pluginId + "\"]");

            plugins.each(function () {
              var _this = $(this);

              _this.triggerHandler('close.zf.trigger', [_this]);
            });
          });
        }
      }

      function resizeListener(debounce) {
        var timer = void 0,
            $nodes = $('[data-resize]');
        if ($nodes.length) {
          $(window).off('resize.zf.trigger').on('resize.zf.trigger', function (e) {
            if (timer) {
              clearTimeout(timer);
            }

            timer = setTimeout(function () {

              if (!MutationObserver) {
                //fallback for IE 9
                $nodes.each(function () {
                  $(this).triggerHandler('resizeme.zf.trigger');
                });
              }
              //trigger all listening elements and signal a resize event
              $nodes.attr('data-events', "resize");
            }, debounce || 10); //default time to emit resize event
          });
        }
      }

      function scrollListener(debounce) {
        var timer = void 0,
            $nodes = $('[data-scroll]');
        if ($nodes.length) {
          $(window).off('scroll.zf.trigger').on('scroll.zf.trigger', function (e) {
            if (timer) {
              clearTimeout(timer);
            }

            timer = setTimeout(function () {

              if (!MutationObserver) {
                //fallback for IE 9
                $nodes.each(function () {
                  $(this).triggerHandler('scrollme.zf.trigger');
                });
              }
              //trigger all listening elements and signal a scroll event
              $nodes.attr('data-events', "scroll");
            }, debounce || 10); //default time to emit scroll event
          });
        }
      }

      function eventsListener() {
        if (!MutationObserver) {
          return false;
        }
        var nodes = document.querySelectorAll('[data-resize], [data-scroll], [data-mutate]');

        //element callback
        var listeningElementsMutation = function listeningElementsMutation(mutationRecordsList) {
          var $target = $(mutationRecordsList[0].target);
          //trigger the event handler for the element depending on type
          switch ($target.attr("data-events")) {

            case "resize":
              $target.triggerHandler('resizeme.zf.trigger', [$target]);
              break;

            case "scroll":
              $target.triggerHandler('scrollme.zf.trigger', [$target, window.pageYOffset]);
              break;

            // case "mutate" :
            // console.log('mutate', $target);
            // $target.triggerHandler('mutate.zf.trigger');
            //
            // //make sure we don't get stuck in an infinite loop from sloppy codeing
            // if ($target.index('[data-mutate]') == $("[data-mutate]").length-1) {
            //   domMutationObserver();
            // }
            // break;

            default:
              return false;
            //nothing
          }
        };

        if (nodes.length) {
          //for each element that needs to listen for resizing, scrolling, (or coming soon mutation) add a single observer
          for (var i = 0; i <= nodes.length - 1; i++) {
            var elementObserver = new MutationObserver(listeningElementsMutation);
            elementObserver.observe(nodes[i], { attributes: true, childList: false, characterData: false, subtree: false, attributeFilter: ["data-events"] });
          }
        }
      }

      // ------------------------------------

      // [PH]
      // Foundation.CheckWatchers = checkWatchers;
      Foundation.IHearYou = checkListeners;
      // Foundation.ISeeYou = scrollListener;
      // Foundation.IFeelYou = closemeListener;
    }(jQuery);

    // function domMutationObserver(debounce) {
    //   // !!! This is coming soon and needs more work; not active  !!! //
    //   var timer,
    //   nodes = document.querySelectorAll('[data-mutate]');
    //   //
    //   if (nodes.length) {
    //     // var MutationObserver = (function () {
    //     //   var prefixes = ['WebKit', 'Moz', 'O', 'Ms', ''];
    //     //   for (var i=0; i < prefixes.length; i++) {
    //     //     if (prefixes[i] + 'MutationObserver' in window) {
    //     //       return window[prefixes[i] + 'MutationObserver'];
    //     //     }
    //     //   }
    //     //   return false;
    //     // }());
    //
    //
    //     //for the body, we need to listen for all changes effecting the style and class attributes
    //     var bodyObserver = new MutationObserver(bodyMutation);
    //     bodyObserver.observe(document.body, { attributes: true, childList: true, characterData: false, subtree:true, attributeFilter:["style", "class"]});
    //
    //
    //     //body callback
    //     function bodyMutation(mutate) {
    //       //trigger all listening elements and signal a mutation event
    //       if (timer) { clearTimeout(timer); }
    //
    //       timer = setTimeout(function() {
    //         bodyObserver.disconnect();
    //         $('[data-mutate]').attr('data-events',"mutate");
    //       }, debounce || 150);
    //     }
    //   }
    // }
  }, {}], 10: [function (require, module, exports) {
    'use strict';

    require('./foundation/foundation.core.js');
    require('./foundation/foundation.util.mediaQuery.js');
    require('./foundation/foundation.util.motion.js');
    require('./foundation/foundation.util.triggers.js');
    require('./foundation/foundation.util.keyboard.js');
    require('./foundation/foundation.util.touch.js');
    require('./foundation/foundation.slider.js');
    var ColorPalette = require('./colorPalette.js');

    $(document).ready(function () {
      $(document).foundation();
      window.colorPaletteObject = new ColorPalette('9c27b0', $('#colorPalette'));
    });
  }, { "./colorPalette.js": 1, "./foundation/foundation.core.js": 3, "./foundation/foundation.slider.js": 4, "./foundation/foundation.util.keyboard.js": 5, "./foundation/foundation.util.mediaQuery.js": 6, "./foundation/foundation.util.motion.js": 7, "./foundation/foundation.util.touch.js": 8, "./foundation/foundation.util.triggers.js": 9 }] }, {}, [10]);